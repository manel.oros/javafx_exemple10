package dam2.mavenproject4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    // escena de la vista principal
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        
        // carreguem la vista principal
        scene = new Scene(loadFXML("base"));
            
        //apliquem els estils
        scene.getStylesheets().add(getClass().getResource("estils.css").toExternalForm());
        
        //definim els mínims de la finestra (escenari)
        stage.setMinHeight(1000);
        stage.setMinWidth(1500);
        //apliquem l'escena a l'escenari i la mostrem
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}