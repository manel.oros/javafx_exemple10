package dam2.mavenproject4;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/***
 * controlador base.
 * 
 * A partir d'aquest controller, s'insereixen les altres vistes en la forma de components
 * @author manel
 */
public class BaseController implements Initializable{

    @FXML
    private HBox borderTop;
    @FXML
    private VBox borderLeft;
    @FXML
    private HBox borderDown;
    @FXML
    private FlowPane center;
    @FXML
    private VBox borderRight;

    /***
     * S'omplen les 5 zones amb les vistes element.xml i element2.xml, repetides els cops que es necessiti.
     * 
     *
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
         
        
        try {
            
            // carreguem la vista i l'assignem element 1 a la zona central
            // una vista es carrega en forma del seu node pare (i subnodes) 
            // Llavors aquesta vista la poden assignar a un node existent. En aquest cas a les
            // diferents zones de la vista base
            
            // cada cop que carreguem una mateixa vista amb el mètode load, realment
            // estem generant una instància nova del controller
            Node vistaCentral1 = FXMLLoader.load(App.class.getResource("element.fxml"));
            Node vistaCentral2 = FXMLLoader.load(App.class.getResource("element.fxml"));
            Node vistaCentral3 = FXMLLoader.load(App.class.getResource("element.fxml"));
            Node vistaCentral4 = FXMLLoader.load(App.class.getResource("element.fxml"));
            Node vistaCentral5 = FXMLLoader.load(App.class.getResource("element.fxml"));
            Node vistaCentral6 = FXMLLoader.load(App.class.getResource("element.fxml"));
            
            // inserim varis cops la vista a la zona central
            center.getChildren().add(vistaCentral1);
            center.getChildren().add(vistaCentral2);
            center.getChildren().add(vistaCentral3);
            center.getChildren().add(vistaCentral4);
            center.getChildren().add(vistaCentral5);
            center.getChildren().add(vistaCentral6);
            
            //ara inserim la vista "element2" varis cops a la part lateral dreta (amb menys línies de codi)
            for (int i = 0; i< 7; i++)
                borderRight.getChildren().add(FXMLLoader.load(App.class.getResource("element2.fxml")));
            
            //idem a la part esquerra
            for (int i = 0; i< 8; i++)
                borderLeft.getChildren().add(FXMLLoader.load(App.class.getResource("element2.fxml")));
            
            //idem a la part inferior
            for (int i = 0; i< 10; i++)
                borderDown.getChildren().add(FXMLLoader.load(App.class.getResource("element.fxml")));
            
            //idem a la part superior
            for (int i = 0; i< 8; i++)
                borderTop.getChildren().add(FXMLLoader.load(App.class.getResource("element.fxml")));

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}