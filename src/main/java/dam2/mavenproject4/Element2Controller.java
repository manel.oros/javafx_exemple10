/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package dam2.mavenproject4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author manel
 */
public class Element2Controller {
    
   
    @FXML
    private Button btn2;

    @FXML
    private Label lbl1;

    @FXML
    private Button btn1;

    @FXML
    void onActionButton(ActionEvent event) {
        
        Button b = (Button)event.getSource();
        
        lbl1.setText(b.getId() + " !!");

    }
    
}
